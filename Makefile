FILES=$(shell find -iname '*.js' -and -not \( -name 'run.js' -or -path ./third_party/\* \))

.PHONY: test
test:
	@$(if $(COVERAGE),GJS_COVERAGE_PREFIXES=$(shell pwd) GJS_COVERAGE_OUTPUT=.coverage,) $(firstword $(JASMINE) jasmine) $(if $(MAKE_TERMOUT),--color,--no-color) $(if $(V),--verbose,)
	@$(if $(COVERAGE),lcov -r .coverage/coverage.lcov "**/third_party/**/*" "**/unit/**/*" "**/spec/*" -o .coverage/coverage.lcov)

.coverage/coverage.lcov: COVERAGE=1
.coverage/coverage.lcov: test

html-coverage: .coverage/coverage.lcov
	@rm -rf coverage
	@$(firstword $(GEN_HTML) genhtml) --output-directory coverage --branch-coverage .coverage/coverage.lcov

clean:
	@find \( -name '*.rej' -or -name '*.orig' -or -name '*.porig' \) -exec rm -v '{}' \;

realclean: clean
	@rm -rvf third_party

check:
	@$(if $(shell command -v $(ESLINT) 2> /dev/null),$(ESLINT),\
		$(if $(shell command -v eslint 2> /dev/null),eslint,\
			$(if $(shell command -v npx 2> /dev/null),npx eslint,\
				$(if $(shell command -v npm 2> /dev/null),$(shell npm bin)/eslint,\
					"**Eslint not found**"\
				)\
			)\
		)\
	)\
	$(if $(FIX),--fix,) $(FILES)

beautify: FIX=1
beautify: check
